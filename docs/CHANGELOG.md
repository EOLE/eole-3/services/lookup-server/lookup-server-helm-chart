# Changelog

### [1.2.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/lookup-server-helm-chart/compare/release/1.2.0...release/1.2.1) (2025-01-30)


### Bug Fixes

* update appVersion to 1.4.0 ([caa76f9](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/lookup-server-helm-chart/commit/caa76f96184d1962f47ff7ca693f33fee3c17c87))

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/lookup-server-helm-chart/compare/release/1.1.0...release/1.2.0) (2024-10-28)


### Features

* update appVersion to 1.2.0 ([0acb347](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/lookup-server-helm-chart/commit/0acb34762cf471d1e9d1891856d10886f3f9e9bb))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/lookup-server-helm-chart/compare/release/1.0.1...release/1.1.0) (2023-10-17)


### Features

* add lookup server api key env var ([ed21469](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/lookup-server-helm-chart/commit/ed21469125698505be989cd32c87e3c27346650c))


### Bug Fixes

* dev chart version deploy dev app version ([8f2dd90](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/lookup-server-helm-chart/commit/8f2dd90ad70091a28418fa988103352bfab2f1f5))

### [1.0.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/lookup-server-helm-chart/compare/release/1.0.0...release/1.0.1) (2023-05-09)


### Bug Fixes

* helm testing deploy testing app version ([c0967dc](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/lookup-server-helm-chart/commit/c0967dcc3f8119a6cf94c4cf1b41571be877db0a))
* publish new helm stable version ([f81f570](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/lookup-server-helm-chart/commit/f81f570ee08267f7ae7ee9c98b98bd74e4bbb12e))

## 1.0.0 (2022-12-09)


### Features

* **ci:** build helm package ([d949d62](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/commit/d949d6263ba56f7d9cf3d4327f694ef7e1696b7f))
* **ci:** push helm package to chart repository ([a5c3435](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/commit/a5c3435ff7cc00993bda36ad682d722e6271bb3d))
* **ci:** update helm chart version with `semantic-release` ([9774cae](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/commit/9774cae6d612bd8304f6f51ce8b2ad273627fba9))
* **ci:** validate helm chart at `lint` stage ([72dd42f](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/commit/72dd42f61f1633390049506e291ad82e2370d3c1))
* use chart.yaml as default version ([1d979e5](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/commit/1d979e5a3c20aa56e1f9aaac9b96164df2a9f795))


### Bug Fixes

* force rebuild chart bis ([1733392](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/commit/17333929d0757c1febc760105bcd8fea03294f8e))
* force rebuild chart for image repository default value ([4be562e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/commit/4be562e344353f4a7498489d187b2dc33a5fc7fe))
* **lookup-server:** add containerport default value ([3a33aab](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/commit/3a33aabad5064f250f0d2c8ac0449a1f97546c66))
* **lookup-server:** add image repository default value ([1edd861](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/commit/1edd86134c6da6d2ace3bcdf166dca2c839d5f69))
* **lookup-server:** force rebuild chart for image repository default value ([a2599f1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/commit/a2599f16a4a18241aae36200cc1c248250c92c9e))
* update hpa api version removed on k8s 1.26 ([336bba6](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/commit/336bba6e41daa0ebf7e81c4e6f2b1448a4600fec))


### Continuous Integration

* **commitlint:** enforce commit message format ([9c877be](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/commit/9c877bea3a4df2b94410eaa869a064dadd61320f))
* **release:** create release automatically with `semantic-release` ([10458c3](https://gitlab.mim-libre.fr/EOLE/eole-3/services/lookup-server/commit/10458c3232918f965648c73abb908401e3c0ac52))
